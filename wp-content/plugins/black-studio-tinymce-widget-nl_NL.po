# Translation of Plugins - Black Studio TinyMCE Widget - Stable (latest release) in Dutch
# This file is distributed under the same license as the Plugins - Black Studio TinyMCE Widget - Stable (latest release) package.
msgid ""
msgstr ""
"PO-Revision-Date: 2017-06-03 08:29:47+0000\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: GlotPress/2.4.0-alpha\n"
"Language: nl\n"
"Project-Id-Version: Plugins - Black Studio TinyMCE Widget - Stable (latest release)\n"

#. Author URI of the plugin/theme
msgid "http://www.blackstudio.it"
msgstr "http://www.blackstudio.it"

#. Author of the plugin/theme
msgid "Black Studio"
msgstr "Black Studio"

#. Description of the plugin/theme
msgid "Adds a new \"Visual Editor\" widget type based on the native WordPress TinyMCE editor."
msgstr "Voegt een nieuwe \"Visuele Editor\" widget toe, die is gebaseerd op de standaard WordPress TinyMCE editor."

#. Plugin URI of the plugin/theme
msgid "https://wordpress.org/plugins/black-studio-tinymce-widget/"
msgstr "https://nl.wordpress.org/plugins/black-studio-tinymce-widget/"

#: includes/class-widget.php:135
msgid "Automatically add paragraphs"
msgstr "Voeg automatisch paragrafen toe"

#: includes/class-widget.php:128
msgid "Title:"
msgstr "Titel:"

#. translators: description of the widget, shown in available widgets
#: includes/class-widget.php:29
msgid "Arbitrary text or HTML with visual editor"
msgstr "Arbitraire tekst of HTML met visuele editor"

#. translators: title of the widget
#: includes/class-widget.php:27
msgid "Visual Editor"
msgstr "Visuele bewerker"

#: includes/class-compatibility-wordpress.php:335
msgid "Visual"
msgstr "Visueel"

#: includes/class-compatibility-wordpress.php:334
msgid "HTML"
msgstr "HTML"

#. translators: text used for the icon that shows the plugin links
#: includes/class-admin.php:368
msgid "About Black Studio TinyMCE Widget plugin"
msgstr "Over Black Studio TinyMCE Widget plugin"

#. translators: text used for donation link
#: includes/class-admin.php:348
msgid "Donate"
msgstr "Doneren"

#. translators: text used for follow on twitter link
#: includes/class-admin.php:346
msgid "Follow"
msgstr "Volgen"

#. translators: text used for reviews link
#: includes/class-admin.php:344
msgid "Rate"
msgstr "Beoordelen"

#. translators: text used for support forum link
#: includes/class-admin.php:342
msgid "Support"
msgstr "Ondersteuning"

#. translators: text used for support faq link
#: includes/class-admin.php:340
msgid "FAQ"
msgstr "FAQ"

#. translators: text used for plugin home link
#: includes/class-admin.php:338
msgid "Home"
msgstr "Home"

#. translators: error message shown when a duplicated widget ID is detected
#: includes/class-admin.php:250
msgid "ERROR: Duplicate widget ID detected. To avoid content loss, please create a new widget with the same content and then delete this one."
msgstr "FOUT: Duplicate widget ID gedetecteerd. Om inhoud verlies te voorkomen, maak dan een nieuwe widget met dezelfde inhoud en verwijder deze."

#. translators: text for the dismissable admin pointer tooltip
#: includes/class-admin-pointer.php:167
msgid "The Visual Editor widget allows you to insert rich text and media objects in your sidebars"
msgstr "De Visuele Editor geeft je de mogelijkheid om visuele tekst en media-objecten in je sidebars te plaatsen."

#. #-#-#-#-#  black-studio-tinymce-widget-code.pot (Black Studio TinyMCE Widget
#. 2.5.1)  #-#-#-#-#
#. translators: title for the dismissable admin pointer tooltip (same as plugin
#. name)
#. #-#-#-#-#  black-studio-tinymce-widget-code.pot (Black Studio TinyMCE Widget
#. 2.5.1)  #-#-#-#-#
#. Plugin Name of the plugin/theme
#: includes/class-admin-pointer.php:165
msgid "Black Studio TinyMCE Widget"
msgstr "Black Studio TinyMCE Widget"

#. translators: error message shown when multiple instance of the plugin are
#. detected
#: black-studio-tinymce-widget.php:279
msgid "ERROR: Multiple instances of the Black Studio TinyMCE Widget plugin were detected. Please activate only one instance at a time."
msgstr "FOUT: Meerdere exemplaren van de Black Studio TinyMCE Widget plugin gedetecteerd. Activeer slechts 1 exemplaar tegelijkertijd."

#: black-studio-tinymce-widget.php:193 includes/class-admin-pointer.php:63
#: includes/class-admin.php:72 includes/class-compatibility-plugins.php:65
#: includes/class-compatibility-wordpress.php:65
#: includes/class-compatibility.php:95 includes/class-text-filters.php:71
msgid "Cheatin&#8217; uh?"
msgstr "Valsspelen he?"